<?php
/**
 * Name: Default Apps
 * Description: Force app installation and basic configuration based on service class.
 * Version: 1.1
 * Author: DM42.Net, LLC
 * Maintainer: devhubzilla@dm42.net
 * MinVersion: 3.7.4
 */

use Zotlabs\Lib\Apps;
use Zotlabs\Lib\Libsync;

function defaultapps_load() { 
        Zotlabs\Extend\Hook::register('channel_settings', 'addon/defaultapps/defaultapps.php', 'Defaultapps::channel_settings',1,1); 
	Zotlabs\Extend\Hook::register('construct_page', 'addon/defaultapps/defaultapps.php', 'Defaultapps::channel_settings',1,30000);
}

function defaultapps_unload() { 
        Zotlabs\Extend\Hook::unregister_by_file('addon/defaultapps/defaultapps.php'); 
 
}

class Defaultapps {

	static public function channel_settings() {

		if (!local_channel()) {
			return;
		}

		$uid = local_channel();

		$default_system_apps = service_class_fetch(local_channel(),'default_system_apps');

		if (is_array($default_system_apps)) {
		        $apps = q("select * from app where app_channel = 0");
			$appinfo = [];
			foreach ($apps as $app) {
				$appinfo[$app['app_name']] = $app;
			}

			foreach ($default_system_apps as $appname => $defaults) {
				if (!isset($appinfo[$appname])) {
					logger ('System app ('.$appname.') does not exist. Cannot set as default.');
					continue;
				}
				if (!Apps::system_app_installed(local_channel(),$appname,true) &&
					isset($appinfo[$appname])) {

					$theapp = Apps::app_encode($appinfo[$appname]);
					$theapp['type']='system';
					$a = Apps::app_install(local_channel(),$theapp);
				}
				if (is_array($defaults)) {

					foreach ($defaults as $feature=>$settings) {
						foreach ($settings as $k => $v) {
							$setting = get_pconfig(local_channel(),$feature,$k);
							if (!$setting) {
								set_pconfig(local_channel(),$feature,$k,$v);
							}
						}
					}
				}
			}
		
			$featured_system_apps = service_class_fetch(local_channel(),'featured_system_apps');
			if (is_array($featured_system_apps)) {
				$term='nav_featured_app';
				foreach ($featured_system_apps as $appname) {
					$r = q("select id from app where app_name = '%s' and app_channel = %d limit 1",
							dbesc($appname),
							intval($uid));
					$x = q("delete from term where otype = %d and oid = %d and term = '%s'",
							intval(TERM_OBJ_APP),
							intval($r[0]['id']),
							$term);
					store_item_tag($uid, $r[0]['id'], TERM_OBJ_APP, TERM_CATEGORY, $term, escape_tags(z_root() . '/apps/?f=&cat=' . $term));
				}
			}

			$pinned_system_apps = service_class_fetch(local_channel(),'pinned_system_apps');
			if (is_array($pinned_system_apps)) {
				$term='nav_pinned_app';
				foreach ($pinned_system_apps as $appname) {
					$r = q("select id from app where app_name = '%s' and app_channel = %d limit 1",
							dbesc($appname),
							intval($uid));
					$x = q("delete from term where otype = %d and oid = %d and term = '%s'",
							intval(TERM_OBJ_APP),
							intval($r[0]['id']),
							$term);
					store_item_tag($uid, $r[0]['id'], TERM_OBJ_APP, TERM_CATEGORY, $term, escape_tags(z_root() . '/apps/?f=&cat=' . $term));
				}
			}
		}

		$default_addons = service_class_fetch(local_channel(),'default_addons');
		if (is_array($default_addons)) {
		        $apps = Apps::get_system_apps(false);

			$appinfo = Array();
			foreach ($apps as $app) {
				if ($app['plugin'] == '') {
					continue;
				}
				$app['guid'] = hash('whirlpool',$app['name']);
				$appinfo[$app['plugin']] = $app;
			}

			foreach ($default_addons as $addonname => $defaults) {
				if (!plugin_is_installed($addonname)) {
					logger ('Addon ('.$addonname.') not installed.  Cannot set as default.');
					continue;
				}

				if (!Apps::addon_app_installed(local_channel(),$appinfo['addonname'],true)) {
					Apps::app_install(local_channel(),$appinfo[$addonname],true);
				}

				if (is_array($defaults)) {
					foreach ($defaults as $feature=>$settings) {
						foreach ($settings as $k => $v) {
							$setting = get_pconfig(local_channel(),$feature,$k);
							if (!$setting) {
								set_pconfig(local_channel(),$feature,$k,$v);
							}
						}
					}
				}
			}
		}

		$featured_addons = service_class_fetch(local_channel(),'featured_addons');
			if (is_array($featured_addons)) {
				$term='nav_featured_app';
				foreach ($featured_addons as $addonname) {
					$r = q("select id from app where app_plugin= '%s' and app_channel = %d limit 1",
							dbesc($addonname),
							intval($uid));
					$x = q("delete from term where otype = %d and oid = %d and term = '%s'",
							intval(TERM_OBJ_APP),
							intval($r[0]['id']),
							$term);
					store_item_tag($uid, $r[0]['id'], TERM_OBJ_APP, TERM_CATEGORY, $term, escape_tags(z_root() . '/apps/?f=&cat=' . $term));
				}
			}
		$pinned_addons = service_class_fetch(local_channel(),'pinned_addons');
logger("PINNED ADDONS: ".print_r($pinned_addons,true));
			if (is_array($pinned_addons)) {
				$term='nav_pinned_app';
				foreach ($pinned_addons as $addonname) {
					$r = q("select id from app where app_plugin= '%s' and app_channel = %d limit 1",
							dbesc($addonname),
							intval($uid));
					$x = q("delete from term where otype = %d and oid = %d and term = '%s'",
							intval(TERM_OBJ_APP),
							intval($r[0]['id']),
							$term);
					store_item_tag($uid, $r[0]['id'], TERM_OBJ_APP, TERM_CATEGORY, $term, escape_tags(z_root() . '/apps/?f=&cat=' . $term));
				}
			}

		$default_connections = service_class_fetch(local_channel(),'default_channels');
		$dc_channel = \App::get_channel();
		$dc_uid = local_channel();

		if (is_array($default_connections)) {
			require_once('include/follow.php');
			foreach ($default_connections as $url) {
				$url = notags(trim(punify($url)));
				$dc_done = get_pconfig(local_channel(),'defaultapps_default_channels',$url);
				if ($dc_done==1) {
					continue;
				}
				
				$result = new_contact($dc_uid,$url,$dc_channel,0,0);
				if ($result['success'] != false) {
					set_pconfig(local_channel(),'defaultapps_default_channels',$url,1);


                			$clone = array();
                			foreach($result['abook'] as $k => $v) {
                        			if(strpos($k,'abook_') === 0) {
                                			$clone[$k] = $v;
                        			}
                			}
                			unset($clone['abook_id']);
                			unset($clone['abook_account']);
                			unset($clone['abook_channel']);
		
                			$abconfig = load_abconfig($channel['channel_id'],$clone['abook_xchan']);
                			if($abconfig)
                        			$clone['abconfig'] = $abconfig;
		
                			Libsync::build_sync_packet(0 /* use the current local_channel */, array('abook' => array($clone)), true);

					$can_view_stream = intval(get_abconfig($channel['channel_id'],$clone['abook_xchan'],'their_perms','view_stream'));

                			// If we can view their stream, pull in some posts

                			if(($can_view_stream) || ($result['abook']['xchan_network'] === 'rss'))
                        		\Zotlabs\Daemon\Master::Summon(array('Onepoll',$result['abook']['abook_id']));


				}
			}
		}
	}

}

$defaultappsclass = new Defaultapps();
